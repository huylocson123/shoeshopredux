import './App.css';

import ShoeShopRedux from './ShoeShop/ShoeShopRedux';
function App() {
  return (
    <div className="App">
      <ShoeShopRedux />
    </div>
  );
}

export default App;
