import { shoeArr } from "../../dataShoeShop";
import { ADD_TO_CARD, CHANGE_QUANTITY, DELETE_ITEM } from "../constant/shoeShopConstant";

let initialState = {
    shoeArr: shoeArr,
    gioHang: [],
};
export const shoeShopReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case ADD_TO_CARD: {
            let index = state.gioHang.findIndex((item) => {
                return item.id == payload.id;
            });
            let cloneGioHang = [...state.gioHang];
            if (index == -1) {
                let newSp = { ...payload, soLuong: 1 };
                cloneGioHang.push(newSp);
            } else {
                cloneGioHang[index].soLuong++;
            }
            state.gioHang = cloneGioHang;
            return { ...state };
        }
        case DELETE_ITEM: {
            let index = state.gioHang.findIndex((item) => {
                return item.id == payload;
            });
            if (index !== -1) {
                let cloneGioHang = [...state.gioHang];
                cloneGioHang.splice(index, 1);
                state.gioHang = cloneGioHang;
            } return { ...state }

        }
        case CHANGE_QUANTITY: {
            let index = state.gioHang.findIndex((item) => {
                return item.id == payload.id;
            });
            let cloneGioHang = [...state.gioHang];

            cloneGioHang[index].soLuong = cloneGioHang[index].soLuong + payload.step;
            if (cloneGioHang[index].soLuong == 0) {
                cloneGioHang.splice(index, 1);
            }
            state.gioHang = cloneGioHang;
            return { ...state }
        }
        default: return state;
    }
}
