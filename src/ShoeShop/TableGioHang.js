import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_QUANTITY, DELETE_ITEM } from "./redux/constant/shoeShopConstant";
class TableGioHang extends Component {
    renderContent = () => {
        return this.props.cart.map((item) => {
            return (
                <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.price * item.soLuong}</td>
                    <td>
                        <button
                            onClick={() => {
                                this.props.handleChangeQuantity(item.id, -1);
                            }}
                            className="btn btn-secondary">
                            -
                        </button>
                        <span className="mx-2"> {item.soLuong}</span>
                        <button
                            onClick={() => {
                                this.props.handleChangeQuantity(item.id, 1);
                            }}
                            className="btn btn-warning">
                            +
                        </button>
                    </td>
                    <td>
                        <button
                            onClick={() => {
                                this.props.handleRemoveShoe(item.id);
                            }}
                            className="btn btn-danger">
                            Xoá
                        </button>
                    </td>
                </tr>
            );
        });
    };
    render() {
        return (
            <div>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>{this.renderContent()}</tbody>
                </table>
            </div>
        );
    }
}
let mapStateToProps = (state) => {
    return {
        cart: state.shoeShopReducer.gioHang,
    };
};
let mapDispatchToProps = (dispatch) => {
    return {
        handleRemoveShoe: (id) => {
            dispatch({
                type: DELETE_ITEM,
                payload: id,
            })

        },
        handleChangeQuantity: (id, step) => {
            dispatch({
                type: CHANGE_QUANTITY,
                payload: {
                    id,
                    step,
                }
            })
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TableGioHang)
